import React from 'react';
import {Provider} from 'react-redux';
import {createStoreWithMiddleware} from './src/store/configureStore';
import TodoScreen from './src/screens/TodoScreen';

const App = () => (
    <Provider store={createStoreWithMiddleware}>
        <TodoScreen/>
    </Provider>
);

export default App;
