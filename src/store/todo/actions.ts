import {QUERY, Todo, TodoActionTypes} from './types';
import API, {graphqlOperation} from '@aws-amplify/api';
import {createTodo} from '../../graphql/mutations';
import {listTodos} from '../../graphql/queries';
import config from '../../../aws-exports';

API.configure(config);

export function getTodos(): TodoActionTypes {
    return API.graphql(graphqlOperation(listTodos))
        .then(todoData => {
            return {
                type: 'QUERY',
                todos: todoData.data.listTodos.items
            }
        })
        .catch(err => {
            console.error('FAILED', err);
                return {
                    type: 'QUERY',
                    todos: [],
                }
            }
        );
}

export function pushTodo(todo: Todo): TodoActionTypes {
    return API.graphql(graphqlOperation(createTodo, {input: {
            name: todo.name,
            description: todo.description,
        }}))
        .then(() => Promise.resolve())
        .catch(err => {
            console.error('FAILED', err);
                return Promise.reject(err);
            }
        );
}
