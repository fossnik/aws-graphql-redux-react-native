export const QUERY = 'QUERY';
export const PUSH_TODO = 'PUSH_TODO';

export interface Todo {
    id: string,
    name: string,
    description: string,
}

interface QueryTodosAction {
    type: typeof QUERY
    payload: Todo[]
}

interface PushTodoAction {
    type: typeof PUSH_TODO
    payload: Todo
}

export type TodoActionTypes = QueryTodosAction;
