import {QUERY, SUBSCRIPTION} from './types';

const initialState = {todos: []};

export const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case QUERY:
            return {...state, todos: action.todos};
        case SUBSCRIPTION:
            return {...state, todos: [...state.todos, action.todo]};
        default:
            return state;
    }
};
