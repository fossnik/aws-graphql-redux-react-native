import {combineReducers} from 'redux';
import {todoReducer} from './todo/todoReducer';

export const rootReducer = combineReducers({
    todos: todoReducer,
});

export type AppState = ReturnType<typeof rootReducer>;
