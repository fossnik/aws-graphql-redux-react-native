import {applyMiddleware, compose, createStore} from 'redux';
import {rootReducer} from './index';
import promise from 'redux-promise';

let composeEnhancers = compose;
if (__DEV__) {
    composeEnhancers = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;
}

export const createStoreWithMiddleware = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(promise))
);
