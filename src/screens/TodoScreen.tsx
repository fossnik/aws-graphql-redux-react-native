import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StyleSheet, Text, View, Button, ViewStyle, TextInput, SafeAreaView} from 'react-native';
import {AppState} from "../store";
import {getTodos, pushTodo} from '../store/todo/actions';
import {Todo} from "../store/todo/types";
import _ from 'lodash';

class TodoScreen extends Component<{
    todos: [],
    getTodos: () => any,
    pushTodo: (Todo) => any,
}> {
    state = {
        newTodoName: '',
        newTodoDesc: '',
    };

    componentDidMount(): void {
        this.props.getTodos();
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.subContainer}>
                    <Text style={{padding: 10}}> List of Todos: </Text>
                    {this.props.todos.length > 0 && _.map(this.props.todos,
                        (todo: Todo, i) =>
                            <Text key={todo.id}>{todo.name} : {todo.description}</Text>
                    )}
                </View>
                <View style={styles.subContainer}>
                    <Text> Create New Todo: </Text>
                    <TextInput
                        value={this.state.newTodoName}
                        // style={styles.input}
                        onChangeText={newTodoName => this.setState({newTodoName})}
                        placeholder={"Name"}
                        // multiline={true}
                        // maxLength={100}
                        editable={true}
                        // autoCapitalize={'characters'}
                    />
                    <TextInput
                        value={this.state.newTodoDesc}
                        // style={styles.input}
                        onChangeText={newTodoDesc => this.setState({newTodoDesc})}
                        placeholder={"Description"}
                        // multiline={true}
                        // maxLength={100}
                        editable={true}
                        // autoCapitalize={'characters'}
                    />
                    <Button onPress={() => this.props.pushTodo({
                        id: Math.random().toString(),
                        name: this.state.newTodoName,
                        description: this.state.newTodoDesc,
                    })} title='Create Todo'/>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create<{
    container: ViewStyle;
    subContainer: ViewStyle;
}>({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    subContainer: {
        padding: 30,
    },
});

const mapStateToProps = (state: AppState) => ({
    todos: state.todos.todos,
});

const mapDispatchToProps = dispatch => ({
    getTodos: () => dispatch(getTodos()),
    pushTodo: (newTodo: Todo) => dispatch(pushTodo(newTodo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoScreen);
